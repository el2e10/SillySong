//
//  ViewController.swift
//  Silly Song
//
//  Created by Jorge Medellin on 2/7/16.
//  Copyright © 2016 Grupo Cascadia. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var nameField: UITextField!
    
    @IBOutlet weak var lyricsView: UITextView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameField.delegate = self
        
    }



    @IBAction func reset(sender: AnyObject) {
        
        nameField.text = ""
        lyricsView.text = ""
        
    }
    
    
    @IBAction func displayLyrics(sender: AnyObject) {
        
        if (self.nameField.text != "") {
            let lyrics = lyricsForName(bananaFanaTemplate, fullName: nameField.text!)
            
            lyricsView.text = lyrics
            
        }
        
    }

}


// UITextFieldDelegate
extension ViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return false
        
    }
}



// join an array of strings into a single template string:
let bananaFanaTemplate = [
    "<FULL_NAME>, <FULL_NAME>, Bo B<SHORT_NAME>",
    "Banana Fana Fo F<SHORT_NAME>",
    "Me My Mo M<SHORT_NAME>",
    "<FULL_NAME>"].joinWithSeparator("\n")




func shortNameFromName(fullName: String) -> String {
    
    let lowercaseName = fullName.lowercaseString
    let vowelSet  = NSCharacterSet(charactersInString: "aeiou")
    
    if let firstVowelRange = lowercaseName.rangeOfCharacterFromSet(vowelSet, options: .CaseInsensitiveSearch) {
        return lowercaseName.substringFromIndex(firstVowelRange.startIndex)
    }
    
    return lowercaseName
    
}



func lyricsForName(lyricsTemplate: String, fullName: String) -> String {
    
    let shortName = shortNameFromName(fullName)
    
    let lyrics = lyricsTemplate
        .stringByReplacingOccurrencesOfString("<FULL_NAME>", withString: fullName)
        .stringByReplacingOccurrencesOfString("<SHORT_NAME>", withString: shortName)

    
    return lyrics
    
}




